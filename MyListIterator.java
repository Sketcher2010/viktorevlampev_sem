import java.util.*;

public class MyListIterator<T> implements ListIterator<T> {
    private Node<T> cur;
    private Node<T> prev;
    private int index;
    private boolean lastOpWasNext;
    private boolean lastOpWasPrev;

    MyListIterator(Node<T> head) {
        cur = head;
        prev = null;
        index = 0;
    }

    public boolean hasNext() {
        return cur != null;
    }
    public boolean hasPrevious() {
        return  prev != null;
    }
    public int nextIndex() {
        return index;
    }
    public int previousIndex() {
        return index-1;
    }
    public T next() throws NoSuchElementException {
        if(!this.hasNext()) {
            throw new NoSuchElementException();
        }
        prev = cur;
        cur = cur.next;
        lastOpWasNext = true;
        lastOpWasPrev = false;
        index++;
        return prev.elem;
    }
    public T previous() throws NoSuchElementException{
        if(!this.hasPrevious())
            throw new NoSuchElementException();
        cur = prev;
        prev = prev.prev;
        lastOpWasNext = false;
        lastOpWasPrev = true;
        index--;
        return cur.elem;
    }
    public void add(T elem) throws IndexOutOfBoundsException {
        Node<T> el = new Node<T>();


    }
    public void set(T elem) throws IllegalStateException {
        Node<T> el = null;
        if(lastOpWasNext) {
            el = prev;
        }
        if(lastOpWasPrev) {
            el = cur;
        }
        if(el == null) throw new IllegalStateException();
        el.elem = elem;
    }
    public void remove() throws UnsupportedOperationException {
        Node<T> el = null;
        if(lastOpWasNext) {
            el = prev;
        }
        if(lastOpWasPrev) {
            el = cur;
        }
        if(el == null) throw new IllegalStateException();
        if(el.prev != null)
            el.prev.next = el.next;
        if(el.next != null)
            el.next.prev = el.prev;
//        не работает
    }
}
