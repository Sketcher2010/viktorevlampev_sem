
import java.util.*;

class Node<T> {
    T elem;
    Node<T> prev;
    Node<T> next;


}

public class ASL<T> extends AbstractSequentialList<T> {
    private Node<T> head;
    public int size;

    ASL(Node<T> head){
        this.head = head;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        MyListIterator<T> it = new MyListIterator<T>(head);
        for (int i = 0; i<index; i++) {
            it.next();
        }
        return it;
    }

    @Override
    public int size() {
        return this.size;
    }
}