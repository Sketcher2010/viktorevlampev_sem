/**
 * Created by Виктор on 17.03.2015.
 */

import java.util.*;

public class elem {
    static <T> Node<T> add(Node<Integer[]> head, Integer[] elem) {
        Node<T> p = new Node<T>();
        p.elem = (T) elem;
        p.prev = null;
        p.next = (Node<T>) head;
        return p;
    }

    public Node<Integer[]> hand(Node<Integer[]>head, Scanner sc) { // ручное заполнение
        System.out.print("Для выхода напишите 3 раза 0 ;-). ");

        int i=1, j=1, a=1;
        while(i!=0 && j != 0 && a != 0) {
            System.out.print("Ваш элемент (i j a): ");
            i = sc.nextInt();
            j = sc.nextInt();
            a = sc.nextInt();

            if(!checkElem(head, new int[] {i, j}))
                head = add(head, new Integer[]{i, j, a});
            else
                System.out.println("Элемент с такими координатами уже есть, нужно будет ввести другой..");
        }
        return head;
    }

    public Node<Integer[]> hpRand(Node<Integer[]>head, Scanner sc) { // полурандомное заполнение
        Random rand = new Random();
        System.out.print("Введите ограничение (матрица у нас квадратная): ");
        int co = sc.nextInt();
        int count = (int)co/5;
        int randCo = rand.nextInt(count);
//        System.out.println("DEBUG: co = "+co+"; count = "+count+"; randCo = "+randCo);
        for (int i = 0; i<randCo; i++) {
            int ii = rand.nextInt(co-1);
            int j = rand.nextInt(co-1);
            int a = rand.nextInt(420);
            if(!checkElem(head, new int[] {ii, j}))
                head = add(head, new Integer[]{ii, j, a});
        }
        return head;
    }

    public Node<Integer[]> rand(Node<Integer[]>head) { // рандомное заполнение
        Random rand = new Random();
        int co = rand.nextInt(100000);
        int count = (int)co/5;
        int randCo = rand.nextInt(count);
//        System.out.println("DEBUG: co = "+co+"; count = "+count+"; randCo = "+randCo);
        for (int i = 0; i<randCo; i++) {
            int ii = rand.nextInt(co-1);
            int j = rand.nextInt(co-1);
            int a = rand.nextInt(420);
            if(!checkElem(head, new int[] {ii, j}))
                head = add(head, new Integer[]{ii, j, a});
        }
        return head;
    }

    public boolean checkElem(Node<Integer[]> node, int[] el) { // возвращает true, если нашеле элемент в матрице с координатами el[0] и el[1]
        boolean res = false;
        int i = 0;
        while(node != null) {
            if(el[0] == node.elem[0] && el[1] == node.elem[1])
                return true;
            node = node.next;
            i++;
        }
        return res;
    }
}
