import java.util.*;

public class prog {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Node<Integer[]> head = null;
        elem elem = new elem();
        int type = 0;
        do {
            System.out.print("Выберете тип создания таблицы. 1 - вводить руками, 2 - ввести размерность, рандомно заполнить, 3 - довериться случаю: ");
            type = sc.nextInt();
            switch (type) {
                case 1: head = elem.hand(head, sc);
                    break;
                case 2: head = elem.hpRand(head, sc);
                    break;
                case 3: head = elem.rand(head);
                    break;
                default:
                    System.out.println("Что за штуку Вы написали?!");
            }
        } while(type < 1 || type > 3);

//        System.out.println("LIST: ");
//        Node<Integer[]> p = head;
//        while(p != null) {
//            System.out.println("i="+p.elem[0]+"; j="+p.elem[1]+"; elem="+p.elem[2]+";");
//            p = p.next;
//        }
//        System.out.println();

        ASL<Integer[]> list = new ASL<Integer[]>(head);

        ListIterator<Integer[]> it = list.listIterator();

//        System.out.println(it.next()[2]);
//        System.out.println(it.next()[2]);
//        it.add(new Integer[] {1, 4, 5});
//        System.out.println(it.previous()[2]);
//        System.out.println();
//        System.out.println();
//        System.out.println();

        for (Integer[] x : list) {
            System.out.println("i="+x[0]+"; j="+x[1]+"; elem="+x[2]+";");
        }
    }
}
